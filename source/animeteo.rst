animeteo package
================

.. automodule:: animeteo.animeteo

animeteo module
------------------------

.. currentmodule:: animeteo.animeteo
.. autoclass:: AniMeteo
   :members:
   :undoc-members:
   :show-inheritance:

georef module
----------------------

.. automodule:: animeteo.georef
   :members:
   :undoc-members:
   :show-inheritance:

trajecto module
------------------------

.. automodule:: animeteo.trajecto
   :members:
   :undoc-members:
   :show-inheritance:
