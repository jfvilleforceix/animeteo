.. Animeteo documentation master file, created by
   sphinx-quickstart on Thu Feb 24 17:39:17 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AniMeteo's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./animeteo.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
